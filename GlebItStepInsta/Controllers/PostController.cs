﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GlebItStepInsta.Areas.Identity.Data;
using GlebItStepInsta.Models;
using GlebItStepInsta.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace GlebItStepInsta.Controllers
{
    public class PostController : Controller
    {
        private readonly IImageUploader imageUploader;
        private readonly ResourcesDbContext resourcesDbContext;
        private readonly UserManager<AppUser> userManager;
        private readonly IDistributedCache cache;

        public PostController(
            IImageUploader imageUploader,
            ResourcesDbContext resourcesDbContext,
            UserManager<AppUser> userManager,
            IDistributedCache cache)
        {
            this.imageUploader = imageUploader;
            this.resourcesDbContext = resourcesDbContext;
            this.userManager = userManager;
            this.cache = cache;
        }

        public IActionResult Details(int id)
        {
            var post = resourcesDbContext.Posts.Find(id);
            return View(post);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(Post post, IFormFile image)
        {
            var appUser = await userManager.GetUserAsync(User);

            post.Photo = await imageUploader.Upload(image);
            post.Date = DateTime.Now;
            post.ProfileId = appUser.ProfileId;

            resourcesDbContext.Posts.Add(post);
            resourcesDbContext.SaveChanges();

            cache.Remove($"posts_{appUser.ProfileId}");

            return RedirectToAction("Index", "Home");
            //return RedirectToAction("UserProfile", "Profile", new { id = appUser.ProfileId });
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            return View();
        }
    }
}