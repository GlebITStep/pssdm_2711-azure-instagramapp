﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GlebItStepInsta.Models;
using GlebItStepInsta.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace GlebItStepInsta.Controllers
{
    public class ProfileController : Controller
    {
        private readonly ResourcesDbContext resourcesDbContext;
        private readonly IDistributedCache cache;

        public ProfileController(
            ResourcesDbContext resourcesDbContext,
            IDistributedCache cache)
        {
            this.resourcesDbContext = resourcesDbContext;
            this.cache = cache;
        }

        public IActionResult UserProfile(int id)
        {
            IEnumerable<Post> posts = null;
            var profile = resourcesDbContext.Profiles.Find(id);

            var json = cache.GetString($"posts_{id}");
            if (!string.IsNullOrEmpty(json))
            {
                posts = JsonConvert.DeserializeObject<IEnumerable<Post>>(json);
            }
            else
            {
                posts = resourcesDbContext.Posts.Where(x => x.ProfileId == id).OrderByDescending(x => x.Date);
                var resultJson = JsonConvert.SerializeObject(posts);
                cache.SetString($"posts_{id}", resultJson);
            }
            return View(new ProfileAndPosts { Profile = profile, Posts = posts });
        }

        public IActionResult Settings(int id)
        {
            return View();
        }
    }
}