﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GlebItStepInsta.Models
{
    public class Profile
    {
        public int Id { get; set; }

        public string AppUserId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Photo { get; set; }
    }
}
