﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GlebItStepInsta.Models
{
    public class Post
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }

        [Required]
        public string Title { get; set; }
        public string Photo { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}
