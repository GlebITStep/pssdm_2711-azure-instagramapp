﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace GlebItStepInsta.Services
{
    public class AzureBlobStorageImageUploader : IImageUploader
    {
        public string AccountName { get; set; } = "instagramdata";
        public string AccountKey { get; set; } = "656W4nWfSYuJGNZycrkAbzQ6rBdUtt+2XvSrFQN2qs0mIAdLojfRHY1ro6K20duLx7cRZODYjLb0JhfEo8PD6g==";

        public async Task<string> Upload(IFormFile file)
        {
            var filename = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";

            // Create storagecredentials object by reading the values from the configuration (appsettings.json)
            StorageCredentials storageCredentials = new StorageCredentials(AccountName, AccountKey);

            // Create cloudstorage account by passing the storagecredentials
            CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, true);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Get reference to the blob container by passing the name by reading the value from the configuration (appsettings.json)
            CloudBlobContainer container = blobClient.GetContainerReference("uploads");

            // Get the reference to the block blob from the container
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

            // Upload the file
            await blockBlob.UploadFromStreamAsync(file.OpenReadStream());

            return filename;
        }
    }
}
