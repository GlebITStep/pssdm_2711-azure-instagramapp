﻿using GlebItStepInsta.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GlebItStepInsta.ViewModels
{
    public class ProfileAndPosts
    {
        public Profile Profile { get; set; }
        public IEnumerable<Post> Posts { get; set; }
    }
}
